from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import Base, engine
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

app = FastAPI()
app.title = "My first API with FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

movies = [
    {
        "id": 1,
        "title": "Avatar",
        "overview": "En un exuberante planeta llamado Pandora viven los nativos",
        "year": "2000",
        "rating": 7.8,
        "category": "Action",
    },
    {
        "id": 2,
        "title": "Kda",
        "overview": "En un exuberante porte hubo un drama",
        "year": "2004",
        "rating": 8.8,
        "category": "Drama",
    },
]


@app.get("/", tags=["home"])
def messsage():
    return HTMLResponse("<h1>Hello world</h1>")
