from pydantic import BaseModel, Field
from fastapi.responses import JSONResponse
from typing import Optional


class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=5, max_length=15)
    overview: str = Field(min_length=15, max_length=250)
    year: int = Field(default=2020, le=2022)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=5, max_length=15)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "title": "Default title",
                "overview": "Description of the movie",
                "year": 2022,
                "rating": 9.8,
                "category": "Action",
            }
        }
